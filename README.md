### Clone this repo with following git command

```
git clone --depth=1 -b master https://gitlab.com/GhostMaster69-dev/packages_apps_GoogleCameraLite.git packages/apps/GoogleCameraLite
```

### Add following package in device tree

```
# Google Camera Lite
PRODUCT_PACKAGES += \
       GoogleCameraLite
```
